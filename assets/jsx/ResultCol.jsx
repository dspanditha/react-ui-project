import React from 'react';
import CardResult from './CardResult.jsx';
import result from '../../assets/files/data.json';


class ResultCol extends React.Component {


    render() {
        return (
            <div>
                {result.results.map((val, key) => <CardResult key={key}
                    data={val} />)}
            </div>


        );
    }
}

export default ResultCol;