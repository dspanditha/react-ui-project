import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

class CardResult extends React.Component {
    render() {
        return (
            <div className="card-block card-custom container m-t-10 m-b-10">
                <table className="col-md-12 m-b-10 format-padding text-left">
                    <tbody>
                        <tr>
                            <td style={{ backgroundColor: this.props.data.agency.brandingColors.primary }} className='header'>
                                <img className='img-thumbnail no-border format-padding' src={this.props.data.agency.logo} />
                            </td>
                        </tr>
                        <tr>
                            <td className="format-padding">
                                <img className='img-thumbnail no-border format-padding' src={this.props.data.mainImage} />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <h4 className="m-b-0">{this.props.data.price}</h4>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="overlay">
                    <button className='add-btn btn btn-success btn-lg' id='${val.id}' ><i className='fa fa-plus'></i> Add Property</button>
                </div>
            </div>

        );
    }
}

export default CardResult;