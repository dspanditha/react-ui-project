import React from 'react';
import CardSave from './CardSave.jsx';
import result from '../../assets/files/data.json';


class ResultCol extends React.Component {


    render() {
        return (
            <div>
                {result.saved.map((val, key) => <CardSave key={key}
                    data={val} />)}
            </div>


        );
    }
}

export default ResultCol;