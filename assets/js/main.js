import React from 'react';
import ReactDOM from 'react-dom';
import ResultCol from '../../assets/jsx/ResultCol.jsx';
import SaveCol from '../../assets/jsx/SaveCol.jsx';


ReactDOM.render(
    <div >
        <div className="card-header bg-gray no-border text-center">
            <h4 className="w-300">Results </h4>
        </div>
        <ResultCol />

    </div>, document.getElementById('app_result')
);

ReactDOM.render(
    <div >
        <div className="card-header bg-gray no-border text-center">
            <h4 className="w-300">Saved </h4>
        </div>
        <SaveCol />

    </div>, document.getElementById('app_save')
);