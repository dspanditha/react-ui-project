var config = {
    entry: './assets/js/main.js',
    output : {
        path     : __dirname + "/dist",
        filename : 'compiled.dist.js'
    },
     
    devServer: {
       inline: true,
       port: 8080
    },
     
    module: {
       loaders: [
          {
             test: /\.jsx?$/,
             exclude: /node_modules/,
             loader: 'babel-loader',
                 
             query: {
                presets: ['es2015', 'react']
             }
          }
       ]
    }
 }
 
 module.exports = config;
 